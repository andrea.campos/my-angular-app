import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { About } from './../models/Iabout';
import { ENDPOINTS } from './../../../../endPoints/endPoints';

@Injectable({
  providedIn: 'root'
})
export class AboutService {
  private aboutUrl: string = ENDPOINTS.about;
  constructor(private http: HttpClient) {}

  public getAbout(): Observable<any> {
    return this.http.get(this.aboutUrl).pipe
    (map((response: About | any) => {
        if (!response) {
          throw new Error('Value expected');

        } else {
          return response;
        }
      }),
      catchError ((error) => {
        throw new Error(error.message);
      })
    );
  }
}
