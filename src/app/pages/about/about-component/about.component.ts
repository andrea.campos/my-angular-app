import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { AboutService } from '../services/about.service';
import { About } from '../models/Iabout';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  public dataAbout: About | any;
  constructor(private location: Location, private aboutService: AboutService) {}

  ngOnInit(): void {
    this.getAboutData();
  }

  public getAboutData(): void {
    this.aboutService.getAbout().subscribe(
      (data: About) => {
        this.dataAbout = data;
      },
      (error) => {
        console.error(error.massage);
      }
    );
  }

  public goBack(): void {
    this.location.back();
  }

}
