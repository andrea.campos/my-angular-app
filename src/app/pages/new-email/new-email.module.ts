import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewEmailRoutingModule } from './new-email-routing.module';
import { NewEmailComponent } from './new-email-component/new-email.component';

@NgModule({
  declarations: [NewEmailComponent],
  imports: [
    CommonModule,
    NewEmailRoutingModule
  ]
})
export class NewEmailModule { }
