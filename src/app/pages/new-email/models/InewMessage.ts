export interface newEmailMessage {
    id: number;
    name: string;
    mail: string;
    age: number;
    message: string;
}