import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { newEmailMessage } from './../models/InewMessage';
import { ENDPOINTS } from './../../../../endPoints/endPoints';


@Injectable({
  providedIn: 'root'
})
export class NewEmailService {
  private newMessageUrl: string = ENDPOINTS.newMessage;


  constructor(private http: HttpClient) {}

  public getNewMessage(): Observable<any> {
    return this.http.get(this.newMessageUrl).pipe(
      map((response: newEmailMessage | any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError ((error) => {
        throw new Error(error.message);
      })
    );
  }

  public postNewMessage(message: newEmailMessage): Observable<any> {
    return this.http.post(this.newMessageUrl, message).pipe(
      map((response: newEmailMessage | any) => {
        if (!response) {
          throw new Error('Value expected');

        } else {
          return response;
        }
      }),
      catchError ((error) => {
        throw new Error(error.message);
      })
    );
  }
}
