import { NewEmailService } from './../../new-email/services/new-email.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';

import { ContactForm } from '../models/Icontact';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  public contactForm: FormGroup;
  public submitted = false;
  public idCounter: number = 100;

  constructor(
    private location: Location,
    private formBuilder: FormBuilder,
    private newEmailService: NewEmailService
  ) {
    this.contactForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(1)]],
      mail: ['', [Validators.required, Validators.email]],
      age: ['', [Validators.required, Validators.min(18)]],
      message: ['', [Validators.required, Validators.maxLength(100)]],
    });
  }

  ngOnInit(): void { }

  public onSubmit(): void {
    this.submitted = true;
    if (this.contactForm.valid) {
      this.postNewMessage();
      this.contactForm.reset();
      this.idCounter = this.idCounter + 1;
    }
  }

  public postNewMessage(): void {
    const myMessage: ContactForm = {
      id: this.contactForm.get('id')?.value,
      name: this.contactForm.get('name')?.value,
      mail: this.contactForm.get('mail')?.value,
      age: this.contactForm.get('age')?.value,
      message: this.contactForm.get('message')?.value,
    };
    this.newEmailService.postNewMessage(myMessage).subscribe(
      (data) => {
        console.log(data);
      },
      (error) => {
        console.error(error.message);
      }
    );
  }

  public goBack(): void {
    this.location.back();
  }
}
