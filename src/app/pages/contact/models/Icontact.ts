export interface ContactForm {
    id: number;
    name: string;
    mail: string;
    age: number;
    message: string;
}