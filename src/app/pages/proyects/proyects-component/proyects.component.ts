import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { ProyectsService } from '../services/proyects.service';
import { Skills } from '../models/Iskills';


@Component({
  selector: 'app-proyects',
  templateUrl: './proyects.component.html',
  styleUrls: ['./proyects.component.scss']
})
export class ProyectsComponent implements OnInit {
  public dataSkills: Skills | any;
  constructor(private location: Location, private skillsService: ProyectsService) {}

  ngOnInit(): void {
    this.getSkillsData();
  }

  public getSkillsData(): void {
    this.skillsService.getSkills().subscribe(
      (data: Skills) => {
        this.dataSkills = data;
      },
      (error) => {
        console.error(error.massage);
      }
    );
  }

  public goBack(): void {
    this.location.back();
  }

}
