export interface Skills {
    id: number;
    img: Img;
    title: string;
    description: string;
}

export interface Img {
    url: string;
    alt: string;
}
