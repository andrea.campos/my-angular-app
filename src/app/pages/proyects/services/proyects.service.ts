import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


import { Skills } from './../models/Iskills';
import { ENDPOINTS } from './../../../../endPoints/endPoints';

@Injectable({
  providedIn: 'root'
})
export class ProyectsService {
  private skillsUrl: string = ENDPOINTS.skills;

  constructor(private http: HttpClient) {
    //empty
  }

  public getSkills(): Observable<any> {
    return this.http.get(this.skillsUrl).pipe
    (map((response: Skills | any) => {
        if (!response) {
          throw new Error('Value expected');

        } else {
          return response;
        }
      }),
      catchError ((error) => {
        throw new Error(error.message);
      })
    );
  }
}
