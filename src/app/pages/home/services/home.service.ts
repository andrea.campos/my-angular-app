import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Home } from './../models/Ihome';
import { ENDPOINTS } from './../../../../endPoints/endPoints';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private homeUrl: string = ENDPOINTS.home;
  constructor(private http: HttpClient) {}

  public getHome(): Observable<any> {
    return this.http.get(this.homeUrl).pipe
    (map((response: Home | any) => {
        if (!response) {
          throw new Error('Value expected');

        } else {
          return response;
        }
      }),
      catchError ((error) => {
        throw new Error(error.message);
      })
    );
  }
  
}
