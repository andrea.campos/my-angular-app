import { Component, OnInit } from '@angular/core';

import { HomeService } from '../services/home.service';
import { Home } from '../models/Ihome';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public dataHome: Home | any;
  constructor(private homeService: HomeService) {}

  ngOnInit(): void {
    this.getHomeData();
  }

  public getHomeData(): void {
    this.homeService.getHome().subscribe(
      (data: Home) => {
        this.dataHome = data;
      },
      (error) => {
        console.error(error.massage);
      }
    );
  }
}
