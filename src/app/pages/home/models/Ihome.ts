export interface Home {
    id: number;
    name: string;
    lastname: string;
    description: string;
}