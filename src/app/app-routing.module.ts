import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
  loadChildren: () => import('./pages/home/home.module').then((m) => m.HomeModule),
  },

  {
    path: '', redirectTo: 'home', pathMatch: 'full',
  },

  {
    path: 'about',
  loadChildren: () => import('./pages/about/about.module').then((m) => m.AboutModule),
  },

  {
    path: 'proyects',
  loadChildren: () => import('./pages/proyects/proyects.module').then((m) => m.ProyectsModule),
  },

  {
    path: 'contact',
  loadChildren: () => import('./pages/contact/contact.module').then((m) => m.ContactModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
