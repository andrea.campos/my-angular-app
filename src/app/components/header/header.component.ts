import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.addListeners();
  }


public addListeners = () => {
    document.getElementById("nav-hamburger")?.addEventListener("click", this.toggleNavDowndrop);
}

public toggleNavDowndrop = () => {
  let downDrop = document.getElementById("mobile-downdrop");
  if(downDrop?.classList.contains('hide')) {
    downDrop.classList.remove('hide');
  } else {
    downDrop?.classList.add('hide');
  }
}

}
