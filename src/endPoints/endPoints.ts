export const ENDPOINTS = {
    home: 'http://localhost:3000/home',
    about:'http://localhost:3000/about',
    skills:'http://localhost:3000/skills',
    newMessage: 'http://localhost:3000/newMessage'
}

// export const ENDPOINTS = {
//     home: 'http://localhost:8000/api/home',
//     about:'http://localhost:8000/api/about',
//     skills:'http://localhost:8000/api/skills',
//     newMessage: 'http://localhost:8000/api/newMessage'
// }
